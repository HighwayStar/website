repo_url: &repo_url https://codeberg.org/Freeyourgadget/Gadgetbridge
edit_uri: https://codeberg.org/Freeyourgadget/website/src/branch/main/docs/
site_url: &site_url https://gadgetbridge.org
site_description: A free and open source Android application for bluetooth devices.
extra:
  # That's because footer is replaced with overrides/main.html,
  # "Made with Mkdocs" text is included with override.
  # So enabling this will make the "Made with Mkdocs" text appear twice.
  generator: false
  constants:
    fdroid: &fdroid https://f-droid.org/app/nodomain.freeyourgadget.gadgetbridge
    git: *repo_url
    weblate: &weblate https://hosted.weblate.org/projects/freeyourgadget/gadgetbridge
    liberapay: &liberapay https://liberapay.com/Gadgetbridge/donate
    fdroid_repo: https://freeyourgadget.codeberg.page/fdroid/repo?fingerprint=CD381ECCC465AB324E21BCC335895615E07E70EE11E9FD1DF3C020C5194F00B2
    matrix: &matrix https://matrix.to/#/%23gadgetbridge:matrix.org
    mastodon: &mastodon https://social.anoxinon.de/@gadgetbridge
  # Custom macros/blocks that specific to this Gadgetbridge documentation.
  # For macros, an additional Mkdocs plugin has used which is called "mkdocs-macros-plugin",
  # it uses Jinja2 library for templating. Macros are defined in "_mics/macros.py".
  widgets:
    # A shortcut to creating links to the git repository with given issue/PR/commit IDs.
    # {repo_url} will be replaced with "repo_url" in Mkdocs configuration.
    #
    # Set "enabled" below to disable rendering of git links, in this case, the references
    # will be left as a plain text without no linking.
    #
    # To reference a issue/PR/commit in any markdown file, use:
    #
    # - {{ "27406e0881"|commit }} --> To reference a commit. The SHA will be truncated to 10 characters as a display text.
    # - {{ 686|issue }}           --> To reference a issue.
    # - {{ 686|issue(35337) }}    --> To reference a issue with given comment ID.
    # - {{ 2953|pull }}           --> To reference a pull request.
    # - {{ 2953|pull(1210070) }}  --> To reference a pull request with given comment ID.
    git_links:
      enabled: true
      config:
        # Issue references
        issue: ":octicons-issue-opened-16: issue #{0}"
        issue_comment: ":octicons-comment-discussion-16: comment on :octicons-issue-opened-16: issue #{0}"
        issue_url: "{repo_url}/issues/{number}"
        issue_comment_url: "{repo_url}/issues/{number}#issuecomment-{comment}"
        issue_hint: reference to Freeyourgadget/Gadgetbridge#{0}
        # Pull request references
        pull: ":octicons-git-pull-request-16: pull request #{0}"
        pull_comment: ":octicons-comment-discussion-16: comment on :octicons-git-pull-request-16: pull request #{0}"
        pull_url: "{repo_url}/pulls/{number}"
        pull_comment_url: "{repo_url}/pulls/{number}#issuecomment-{comment}"
        pull_hint: reference to Freeyourgadget/Gadgetbridge#{0}
        # Commit reference
        commit: ":octicons-git-commit-16: commit {0}"
        commit_url: "{repo_url}/commit/{commit_sha}"
        commit_hint: reference to commit {0} in Freeyourgadget/Gadgetbridge
    # Displays color in a small circle next to the given in hex.
    #
    # Set "enabled" below to disable rendering of color preview, in this case
    # then the color codes will be simply as a inline code block.
    #
    # Use {{ "#ffffff"|color }} in any markdown file to
    # render this macro.
    color:
      enabled: true
      class_name: gb-color
    # Labels that is show under the gadget name, to indicate its support on Gadgetbridge.
    # See also "device_support.yml" for more details.
    #
    # Set "enabled" below to disable rendering of labels.
    label:
      enabled: true
      class_name: gb-label-container
      values:
        feature_poor:
          name: Poorly supported
          icon: material/circle-slice-1
          class_name: gb-label gb-label-4
          description: Missing too many features, it is probably only pairable in Gadgetbridge with few minor settings, so you shouldn't expect more.
        feature_partial:
          name: Partially supported
          icon: material/circle-slice-3
          class_name: gb-label gb-label-2
          description: Missing important features, you might be required using vendor app at some point to manage the gadget.
        feature_most:
          name: Mostly supported
          icon: material/circle-slice-5
          class_name: gb-label gb-label-3
          description: Missing a few features, but it should be enough to cover essential daily tasks.
        feature_high:
          name: Highly supported
          icon: material/check-circle
          class_name: gb-label gb-label-1
          description: All features or at least all major features are supported, which is enough to cover almost all cases.
        feature_unknown:
          name: Unknown support
          icon: material/progress-question
          class_name: gb-label gb-label-5
          description: It is supported by Gadgetbridge, but we don't have information about its working/missing features. You can help us by testing yourself!
        pair_free:
          name: No-vendor pair
          icon: material/lock-open
          class_name: gb-label gb-label-1
          description: Initial pair can be done in Gadgetbridge without needing the proprietary vendor app.
        pair_huami:
          name: Requires Huami token to pair
          icon: material/key
          class_name: gb-label gb-label-2
          link: "../../../basics/pairing/huami-xiaomi-server/"
          description: You need to pair with vendor (Mi Fit, Zepp or Zepp Life) app first, then obtain the authentication key and put into Gadgetbridge. Click to learn more.
        pair_xiaomi:
          name: Requires Xiaomi token to pair
          icon: material/key
          class_name: gb-label gb-label-2
          link: "../../../basics/pairing/huami-xiaomi-server/"
          description: You need to pair with vendor (Xiaomi Wear, Mi Fit, Mi Fitness) app first, then obtain the authentication key and put into Gadgetbridge. Click to learn more.
        pair_fossil:
          name: Requires Fossil token to pair
          icon: material/key
          class_name: gb-label gb-label-2
          link: "../../../basics/pairing/fossil-server/"
          description: You need to pair with vendor (Fossil) app first, then obtain the authentication key and put into Gadgetbridge. Click to learn more.
        pair_nothing_cmf:
          name: Requires CMF token to pair
          icon: material/key
          class_name: gb-label gb-label-2
          link: "../../../basics/pairing/nothing-cmf-server/"
          description: You need to pair with vendor (CMF Watch) app first, then obtain the authentication key and put into Gadgetbridge. Click to learn more.
        pair_unknown:
          name: Pair method unknown
          icon: material/circle-outline
          class_name: gb-label gb-label-5
          description: It is not known if it requires pairing by vendor app or not, you can help us by testing yourself!
        experimental:
          name: Experimental
          icon: material/flask-empty-outline
          class_name: gb-label gb-label-6
          description: As we don't have that device it is not known if it works correctly. You can help us by testing yourself!
        os_zepp:
          hidden: true
        os_xiaomi_protobuf:
          hidden: true
      links:
        firmware_info:
          description: Click to see list of known firmware versions for this gadget.
          icon: material/segment
          class_name: gb-label gb-label-blank
        protocol:
          description: Click to see protocol information for this gadget.
          icon: material/code-tags
          class_name: gb-label gb-label-blank
        language_pack:
          description: Click to see more information about languages for this gadget.
          icon: material/translate
          class_name: gb-label gb-label-blank
        custom_os:
          description: Click to see more about custom OSes for this gadget.
          icon: material/chip
          class_name: gb-label gb-label-blank
  social:
    - icon: simple/codeberg
      link: *repo_url
      name: Codeberg
    - icon: simple/fdroid
      link: *fdroid
      name: F-Droid
    - icon: simple/weblate
      link: *weblate
      name: Weblate
    - icon: simple/liberapay
      link: *liberapay
      name: Liberapay
    - icon: simple/matrix
      link: *matrix
      name: Matrix
    - icon: simple/mastodon
      link: *mastodon
      name: Mastodon
    # - icon: simple/rss
    #   link: !!python/object/apply:os.path.join [*site_url, feed_rss_created.xml]
    #   name: RSS feed (Blog)
