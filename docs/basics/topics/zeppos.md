---
title: Zepp OS gadgets
---

# Zepp OS gadgets

The following list includes all implemented features for Zepp OS devices. The actual supported features can vary for each device.

List of supported gadgets by Gadgetbridge which runs Zepp OS:

<!--
    This is an auto-populated list from gadgets which has "os_zepp" flag,
    see "device_support.yml" file for more details.

    It basically creates a list item for every Zepp OS gadget and 
    changes the casing for setting as display name for links.
    (like "xiaomi_mi_band_1" becomes to "Xiaomi Mi Band 1")

    Then, an anchor ID appends to the end of the page link,
    so when clicked on it, it will point to the related gadget's own section.
-->
{% for key, device in device_support.items() | sort %}
    {%- if "os_zepp" in device.flags %}
        {% set name = key.replace("_", " ").title().replace("Gts", "GTS").replace("Gtr", "GTR") %}
        {% set icon = ' :material-flask-empty-outline:{: title="Experimental" }' if "experimental" in device.flags else "" %}
* [{{ name }}](../../gadgets/wearables/{{ device.vendor }}.md#device__{{ key }}) {{ icon }}
    {% endif -%}
{% endfor %}

## Implemented features

These features are supported by Gadgetbridge and apply to all Zepp OS watches included in this page. Note that actual available features per device depends on the device capabilities.

??? note "List of features supported by Gadgetbridge"

    * Set time
    * World Clocks
    * Activity sync (activity, sleep, workout)
    * Canned Messages (Calls, SMS, Notifications). Request from watch on connection.
    * Alarms - Set / Request / Notification on change from watch
    * Calendar (create, delete, request events, set location)
    * Vibration Patterns (Set only, no request from watch)
    * User Info
    * Camera remote
    * Notifications
        * Send to watch
        * Delete notifications on phone/watch when deleted on the other
        * Calls
        * Answer phone calls on watch
        * Custom notification icons
        * Reply to notifications from watch
    * Workout
        * Start/stop on phone when it starts/stops on watch
        * Send GPS to watch (for watches without GPS)
    * Reminders (set, request from watch)
    * Music
        * Music Info
        * Volume
        * Music buttons
    * Device actions (fall asleep, wake up)
    * Real-time heart rate
    * Real-time steps
    * Find Phone from watch
        * Stop on watch when stopped on phone
        * Toggle between ring / vibrate
    * Battery Info
    * HTTP requests (we handle them selectively - for now only weather)
    * Set and request all configs. Non-extensive list:
        * Display Items / Shortcuts
        * Heart Rate
            * High-accuracy sleep tracking
            * Sleep breathing quality
            * All-day HR measurement, with high and low alerts
            * Stress monitoring, relaxation reminders
            * All-day SPO2 monitoring / low SPO2 alert
        * Fitness goal / goal notification
        * Password
        * Language / react to change on band
        * Screen brightness / timeout
        * Screen On on notifications
        * Distance / temperature unit
        * Inactivity warnings (enabled, schedule, dnd, dnd schedule)
        * Lift wrist (mode, schedule, sensitivity)
        * Always on Display (mode, schedule)
        * Night mode (mode, schedule)
        * Do Not Disturb (mode, schedule)
        * BT connection advertisement
        * 3rd party realtime HR access
        * Activity monitoring
        * Auto-brightness
        * Always-on Display Style
        * Sound and Vibration
            * Alert tone
            * Cover to Mute
            * Volume
            * Vibrate for alerts
            * Crown vibration
            * Text to Speech
        * Workout detection (category, alert, sensitivity)
        * Keep screen on during a workout
        * GPS
            * Presets, bands, combination, satellite search
            * AGPS expiry reminder
            * AGPS updates
        * Sleep mode (sleep screen, smart enable)
        * Long press upper button
        * Short press lower button
        * Control center display items (swipe from top of screen)
        * Wearing direction
        * Offline Voice
    * Install watchfaces
    * Install firmware upgrades (full and partial)
    * Install applications
    * Upload GPX routes
    * Setting watchface from phone
    * Morning Updates
    * Shortcut cards (resident application settings)
    * Contact management
    * Taking screenshots (not supported on Mi Band 7)
    * Stress Sync and charts
    * PAI Sync and charts
    * [Membership / loyalty cards with Catima](../integrations/catima.md)
    * Logs from watch apps
    * Weather (daily, hourly)
    * Sun & Moon
    - Phone silent mode from watch

## Zepp OS 3 issues

Zepp OS 3 is the latest version of Zepp OS, and still has some issues:

* File transfer is not working. This includes:
    * Notification icons
    * GPX uploads
    * AGPS uploads
* Watchface upload is not working ({{ 3445 | issue }})

## Known issues and limitations

!!! note "Open issues"
    Do not forget to check any other [open issues](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues?q=&type=all&sort=&state=open&labels=86709&milestone=0&project=0&assignee=0&poster=0) in the project repository.

* 3rd party realtime heartrate access is not working, even with the vendor app
* Weather is missing some data, due to Gadgetbridge's own limitations
    * Only works for a single location
    * No tide support
* Notification icons require high MTU (enable in device settings)
* Updating reminders database when requesting from band is not implemented
* Sleep data is sometimes off

## Missing features

* App integrations
* Calendar: repeating events, event notification delay
* Heartrate manual measurement from phone
* Music on device memory (needs Wi-Fi / Network Addon)
* Configure date and time format from phone
* Application Management (experimental)
    * Known issues: allows uninstall system apps (dangerous), does not always refresh the UI
* Some configuration options on phone:
    * Wake Up Alarm
    * Sleep Mode
    * Bedtime Reminders

### Missing data synchronization

* Sleep SPO2
* Body temperature (protocol implemented, no UI)
* SPO2 Sync (experimental feature, no UI)
* Resting HR (experimental feature, no UI)
* Max HR (experimental feature, no UI)
* Manual HR measurements (experimental feature, no UI)
* Speel respiratory rate (experimental feature, no UI)

### Missing integrations with built-in apps

* Alexa (experimental feature)
* Female Health / Cycle Tracking
* To-Do List
* Voice memos (needs Wi-Fi / Network Addon)
* Sleep schedule
* Zepp Coach

## More information for developers

Check [Zepp OS extras](../../internals/topics/zeppos.md) to enable experimental features and get more details for developers.
