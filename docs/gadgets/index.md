---
title: Gadgets
icon: material/gamepad
---

# Gadgets

{% set breakdown = get_gadget_breakdown() %}

<span class="gb-gadgets">
    <span class="gb-gadget-count">
    {%- for i in breakdown.all -%}
        <span class="gb-digit-display" data-digit="{{ i }}">
            <span>0</span><span>1</span><span>2</span><span>3</span>
            <span>4</span><span>5</span><span>6</span><span>7</span>
            <span>8</span><span>9</span>
        </span>
    {%- endfor -%}
    </span>
    <span class="gb-gadget-count-text">
        gadgets are currently known to work with Gadgetbridge![^1]
    </span>
</span>

Gadgetbridge supports more types of gadgets than just wearables, and allows you to manage all of them in one place.

In the side menu, the list of supported gadgets is grouped by gadget type and vendor. Click on a vendor to see all supported gadgets from that vendor.

For a quick lookup, you can also use [**:material-magnify: Search**](?q=%20){: target="_self" } box at the top of the page.

If you got lost and trying to learn how to get started with Gadgetbridge see [Basics](../basics/index.md) to pair your first gadget.

!!! note "Device support"
    Support for some of the devices is still a work in progress, and some of them may be without a maintainer.


## Breakdown

* **{{ breakdown.good }} gadgets**{: style="color: var(--gb-status-color-1);" } are working well with Gadgetbridge. Gadgetbridge supports most or all of the features of these gadgets <em>(and sometimes even more than the vendor app)</em>.
* **{{ breakdown.meh }} gadgets**{: style="color: var(--gb-status-color-2);" } are working partially with Gadgetbridge. Gadgetbridge supports some of the features, but for some functionality or configuration you will have to use the vendor app.
* **{{ breakdown.poor }} gadgets**{: style="color: var(--gb-status-color-4);" } can be only paired with Gadgetbridge without features at the moment. If you have one of these gadgets and want to contribute to Gadgetbridge, you can start with these!
* **{{ breakdown.unknown }} gadgets**{: style="color: var(--gb-status-color-5);" } known to work in some fashion, but it is not known how much of the features are supported.
* In total, {{ breakdown.all }} gadget models from {{ breakdown.vendor }} different vendors.

[^1]: The number of supported gadgets includes every gadget that is documented, meaning that there may be undocumented gadgets that still work with Gadgetbridge. A currently unsupported gadget may work without additional development if it is similar (in terms of operating logic) to a gadget that Gadgetbridge already supports.
