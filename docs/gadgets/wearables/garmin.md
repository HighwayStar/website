---
title: Garmin
---

# Garmin

!!! warning "Warning"
    * We have noticed that in some cases the Bluetooth traffic contains some bits of information (`oauth_token`, `oauth_consumer_key`, `oauth_signature`, ...) that might (potentially, we did not test) grant access to the user's garmin account. **Therefore we warn all the users to mind this information when sharing the logs.**
    * User data/configuration is unmanaged, which means that whatever was set in the official app will remain untouched, no matter what is set in Gadgetbridge.

## Vívomove HR {{ device("garmin_vivomove_hr") }}

Support for this gadget was added in {{ 3180|pull }}. Check the references pull request for more about supported features by Gadgetbridge.

It is likely that other devices of the same generation could be added with a relatively minor effort.

Additionally, work is in progress to add support for newer devices like "Vívomove Style" and "Forerunner 245". "Instinct solar" is also reported (see {{ 2280|issue(1278859) }}) to be compatible with this work in progress as well as "Vivoactive 4" (see {{ 3299|issue }}) and "Vivoactive 5" (see {{ 3459|issue }}).

Devices that need the third version of the protocol are: "Instinct Crossover" (see {{ 3252|issue }}) and "Instinct 2" (see {{ 3063|issue }}).

However our analysis indicates that there is no direct correlation between watch models and protocol version, as **the protocol used by a specific device might change with the firmware** version it runs. 
