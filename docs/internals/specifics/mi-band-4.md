---
title: Mi Band 4 Firmware Update
---

# Mi Band 4 Firmware Update

--8<-- "blocks.md:firmware_danger"

## Getting the firmware

Since we may not distribute the firmware, you have to find it elsewhere :( Mi Band 4 firmware updates were never part of Mi Fit, but are only made available via OTA updates.

## Choosing the right firmware version

**At least up to firmware 1.0.5.55, it is possible to use the non-NFC firmware with the NFC variant!**

**It is also probably possible to do the other way (using NFC firmware on the non-NFC model), but we did not test that**

Device Name                      | HW Revisions                          | Codename | Default Language |
---------------------------------|---------------------------------------|----------|------------------|
Mi Band 4                        | V0.25.129.5, V0.25.130.5, V0.25.131.5 | Cinco M  | Chinese          |
Mi Smart Band 4                  | V0.25.17.5, V0.25.18.5, V0.25.19.5    | Cinco M  | English          |
Mi Band 4 NFC                    | V0.25.131.21                          | Cinco L  | Chinese          |

## Installing the firmware

Copy the desired Mi Band 4 `.fw` and `.res` files to your Android device and open the `.fw` file from any file manager on that device. The Gadgetbridge firmware update activity should then be started and guide you through the installation process. Repeat with the `.res` file (you don't have to do this if the previous version you flashed had exactly the same `.res` version).

If you get the error "Element cannot be installed" or "The file format is not supported" try a different file manager. [Amaze](https://f-droid.org/en/packages/com.amaze.filemanager/){: target="_blank" } should work.

## Known firmware versions

### Mi Band 4 without NFC

fw ver   | tested | known issues | res ver | fw-md5 | res-md5 
---------|--------|-------------------|---------|--------|---------
1.0.5.39 | no     | ? | 54 | `94fb3e51ec3afe9895a760b435d8efb8` | `811e694acce59080f874bf932f50115d`
1.0.5.40 | no     | ? | 54 | `df8d5ca412d81bc0f7d3051e0c638111` | `811e694acce59080f874bf932f50115d`
1.0.5.48 | yes    | ? | 55 | `e629a4dcc6130a51162b1b87a993aa2a` | `0417fd1012ed6e89a0079c82350de724`
1.0.5.55 | yes    | ? | 56 | `cef6f0ce5191511922afb7a4d603b3a4` | `9a8bbe5014eed43b7f65a54223030616`
1.0.5.66 | no     | ? | 60 | `4e5dedd1fd69cf60802ea9df5bcb28ce` | `ae6cddeaa8cc04a3948b5264b423a398`
1.0.6.00 | no     | ? | 61 | `cb3248a109a6d37e7e29f36d8308aa43` | `9ef36239e418b9139556073ec2aa6754`
1.0.6.06 | no     | ? | 62 | `3f4dc1d12169ffa2898fb40ff67a464e` | `8393c01139210b427ba24ca8a81ff37c`
1.0.6.12 | no     | ? | 62 | `ea65e095a494442ef55a8caca57bc0df` | `8393c01139210b427ba24ca8a81ff37c`
1.0.6.16 | no     | ? | 62 | `48287cc00586033329e90243313d6185` | `8393c01139210b427ba24ca8a81ff37c`
1.0.6.26 | no     | ? | 65 | `f237c0aee4f7c7fb0b62efec63c16b8c` | `b5a337cbdad2c94e15e81f1be636d3f8`
1.0.7.02 | no     | ? | 66 | `8bad3bb799439b6d05d83a0d81636933` | `1745f524af56b8a79d36726b470e0729`
1.0.7.04 | no     | ? | 66 | `3d9a58f2616f0e35caafa2fd6f743a65` | `1745f524af56b8a79d36726b470e0729`
1.0.7.06 | no     | ? | 68 | `fa1c0064d2b6017d3ec4498df6b261f8` | `faafb3d7cb622004de62f4877beb1021`
1.0.7.10 | no     | ? | 66 | `e8455e686610bcf1cb5195d9784c0f01` | `1745f524af56b8a79d36726b470e0729`
1.0.7.60 | yes    | ? | 83 | `62936854f743d2c40eeb70412a809cb6` | `343b6866d43a6cc0cddafa81e7d0b3e5`
1.0.9.22 | no     | ? | ?? | ??? | ???
1.0.9.68 | yes    | ? | 97 | `8644e754fffcfe19411ad483200cf6d2` | `330c72af36728fff9b57d4e101cca1ea`

### Mi Band 4 with NFC

fw ver   | tested | known issues | res ver | fw-md5 | res-md5 
---------|--------|-------------------|---------|--------|---------
1.0.5.22 | yes    | ? | ? | `69b78e8315861316b79e5a0ea3c14132` | `ac5b111828961d984982eb566bde9b15`

## Custom font

You can flash through Gadgetbridge a custom `.ft` font file which will allow the device to display some emojis and accented characters.

You also need to enable "Use custom font" in your device specific settings.

As always, flash at **your own risk!**

Source: [Geekdoing](https://geekdoing.com/threads/mi-band-4-custom-font-emoticons-accented-letters.1461/){: target="_blank" }, [MyAmazFit.ru](https://myamazfit.ru/threads/mi-band-4-modificirovannyj-shrift-so-smajlikami.1103/){: target="_blank" }
