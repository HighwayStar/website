---
title: Amazfit Bip OS
---

Bip OS is a patched version of the original firmware allowing custom apps to be included in custom res files. The Alipay menu has been replaced by an application launcher menu.

## Features

* Allows changing alarm times on device
* Includes extra apps
    * Calculator
    * Calendar
    * Flashlight
    * Other demos
* SDK to create own apps extra apps (res packer is windows only)

## Links

- BipOS v0.5.2: Russian thread with download links [amazfit.ru](https://myamazfit.ru/threads/bip-mnvolkov-bipos.1010/page-88#post-43081){: target="_blank" }.
- Russian thread with download links on [myamazfit.ru](https://myamazfit.ru/threads/bip-mnvolkov-bipos.1010/){: target="_blank" }.
- English thread with download links on [myamazfit.ru](https://myamazfit.ru/threads/bip-mnvolkov-bipos-en.1087/){: target="_blank" }.
- Repository of application source code on [GitHub](https://github.com/MNVolkov?tab=repositories){: target="_blank" }.
- List of applications:
    - [myamazfit.ru Google translated](https://translate.googleusercontent.com/translate_c?depth=1&rurl=translate.google.com&sl=auto&sp=nmt4&tl=en&u=https://myamazfit.ru/threads/bip-prilozhenija-dlja-bipos-elf.1174/&xid=17259,15700019,15700186,15700191,15700256,15700259,15700262,15700265,15700271,15700283&usg=ALkJrhhuWjTnopepv_4QneBJ73DmVENGeQ){: target="_blank" }
    - [myamazfit.ru Original in RU](https://myamazfit.ru/threads/bip-prilozhenija-dlja-bipos-elf.1174/){: target="_blank" }

## Flashing

Do at your own risk!

Flash in this order:

* `firmware.fw`
* `font.ft`
* `resource.res`

## Tested and working

* `MNVolkov_BipOS_0.5.2_MOD_by_trace_1.1.5.12.fw`
* `BOLD_v6.12_9.ft`
* `MNVolkov_BipOS_0.5.2_Apps_Res_by_Ilgruppotester_for_trace_MNVolkov_MOD_11512_v61_music.res`

## Building own apps

Currently this is not supported on Linux, but compiling works fine. Packaging the resulting .elf files must be done with a windows tool, might run in wine.

## Add application to resource file

```
ResPack -a <output_RES_file> <input_RES_file> files to be added
```

TODO: Test and document the process
