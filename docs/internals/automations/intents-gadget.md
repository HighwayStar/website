---
title: Gadget-specific intents
---

# Gadget-specific intents

## Flipper Zero

To play a sub-GHz file:

```bash
adb shell am broadcast \
	-a "nodomain.freeyourgadget.gadgetbridge.flipper.zero.PLAY_FILE" \
	-e "EXTRA_FILE_PATH" "/ext/subghz/Vehicles/Tesla/charge_650_long.sub" \
	-e "EXTRA_DURATION" "5000"
```

Optionally, you can specify `EXTRA_APP_NAME` with infrared, NFC or any other app. In case of infrared, you also need to add `EXTRA_BUTTON_NAME` with the value of a button of a remote.

Then you can listen for `nodomain.freeyourgadget.gadgetbridge.flipper.zero.PLAY_DONE` intent to know that file has finished playing.

## Bangle.js

See [Espruino's page about intents.](https://www.espruino.com/Gadgetbridge#intents){: target="_blank" }

## Huami gadgets with a button

You can use `nodomain.freeyourgadget.gadgetbridge.ButtonPressed` as an action to listen events when button pressed on the gadget with `button_id` as a variable. To make this work, make sure to set "Send broadcast" in [Device actions](../automations/events.md) first.

## Fossil

### Commute app (firmware 2.x only)

Configure one or more Actions in the device specific settings. These will be your Commute destinations. For example; "Test".

Then use `nodomain.freeyourgadget.gadgetbridge.Q_COMMUTE_MENU` as action name and compare its `extra_action` with "Test", and send a new intent, using `nodomain.freeyourgadget.gadgetbridge.Q_SET_MENU_MESSAGE` as action, with extra values:

* `EXTRA_MESSAGE`: Test action received!
* `EXTRA_FINISHED`: true

You may set `EXTRA_FINISHED` to false if the watch is to wait for more content from the phone, and then send another intent with more content.

### Custom widgets (firmware 2.20r and newer)

Configure one or more custom widgets in Gadgetbridge 0.63.0 or higher and note down the widget numbers as shown on the watch.

Then use `nodomain.freeyourgadget.gadgetbridge.Q_PUSH_CONFIG` as action name, and to set the widget content, add an extra value with `EXTRA_CONFIG_JSON` key and with the value:

```
{"push":{"set":{"widgetCustom0._.config.upper_text":"<TEXT1>","widgetCustom0._.config.lower_text":"<TEXT2>"}}}
```

Where you can change `<TEXT1>` and `<TEXT2>` as you wish.

### Custom widgets (firmware 2.19r and older)

Configure a custom widget in Gadgetbridge and use `nodomain.freeyourgadget.gadgetbridge.Q_SET_WIDGET_CONTENT` as action name, and to set the widget content, add an extra value with `EXTRA_WIDGET_ID_temp` key and with the value, for example: `21°C`.

### Custom menu

Gadgetbridge 0.67.0 introduced a new custom menu functionality for the Fossil/Skagen HR watches. The setup and usage of this custom menu is considered applicable for advanced users only, so no direct support was built into Gadgetbridge. Instead, a separate [Fossil HR Menu Companion App](https://github.com/dakhnod/Fossil-HR-Menu-Companion){: target="_blank" } was introduced to configure the custom menu.

Because the custom menu is integrated in the [Fossil HR Gadgetbridge watchface](https://codeberg.org/Freeyourgadget/fossil-hr-gbapps){: target="_blank" } code, it is necessary to re-upload the watchface to the watch using Gadgetbridge 0.67.0 or newer. Watchfaces uploaded with older Gadgetbridge versions do not contain the custom menu functionality yet.

When a custom menu action is configured to send data to the phone, it will do so using a commute app event. Refer to [Commute app](#commute-app) for details on how to handle these events.

Custom text can be shown on the left side of custom menu screens. To do so, send a broadcast Intent using one of the apps mentioned before, in this format:

* Action: `nodomain.freeyourgadget.gadgetbridge.Q_PUSH_CONFIG`
* Extra:
    * `EXTRA_CONFIG_JSON` with value `{"push":{"set":{"customWatchFace._.config.response":{"message":"text here","is_finished":true}}}}`

The physical menu buttons configured in the device specific settings in Gadgetbridge will override the watchface-level buttons configuration from the Menu Companion app, so they need to be set to "Nothing" in Gadgetbridge if they are configured in the custom menu.

#### Custom menu embedding

As stated above, the HR Menu Companion is used to send the custom menu structure to the watch. The menu structure will be lost on the watch after the watch crashes or reboots. As of recently, GB will remember that structure and embed it in the watchfaces that are installed henceforth for persistence. So, to make the menu configuration on the watch permanent follow these steps:

* Build the menu using the "HR Menu Companion" app
* Send the menu structure to the watch from the companion
* Test the menu on the watch
* Edit your watchface in Gadgetbridge and (re-)install it on the watch. The new watchface version should be above 1.10
* Done. The menu configuration is now baked into the watchface

### Automatic watchface switching

The watch can contain multiple watchfaces (limited to 2 on firmware 3.0 and up). It is possible to switch between these watchfaces via the app manager by tapping on them and choosing "Activate". Starting with Gadgetbridge 0.68.0 it is also possible to automate this step by sending an Intent from an automation app.

Use the following contents in a broadcast Intent to switch watchface:

* Action: `nodomain.freeyourgadget.gadgetbridge.Q_SWITCH_WATCHFACE`
* Extra:
    * `WATCHFACE_NAME` with value `NewWatchface`

<!-- TODO:

Should we document these default message names?
* nodomain.freeyourgadget.gadgetbridge.FellAsleep
* nodomain.freeyourgadget.gadgetbridge.WokeUp
* nodomain.freeyourgadget.gadgetbridge.StartNonWear

-->

<!-- TODO: https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Huami-Button-Actions#broadcast-message -->
<!-- TODO: https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Huami-Device-Actions#using-broadcast-messages -->
<!-- TODO: https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Fossil-Hybrid-HR#user-content-using-tasker-easer-or-another-automation-app -->
