---
title: Activity analysis
---

# Activity analysis

Gadgetbridge currently stores raw data in its database. Raw data has to be read as "data as sent by the device": what kind of processing is done within each device is beyond our knowledge.

Actigraphy even with pulse oximetry has very limited ability to accurately Eg r = 0.15 for a more elaborate device than any supported by Gadgetbridge vs in lab sleep study (Penzel, Kesper,  Pinnow, Becker, & Vogelmeier, 2004). Though better in this systematic analysis (Yalamanchali, Farajian, Hamilton, Pott, Samuelson, & Friedman, 2013).

## Issue tracker topics

There are **many** issues in the tracker about this. Even if you simply [sort issues by "most commented"][most-commented-issues]{: target="_blank" }, you will get those interesting discussions. Here is one of them, with some tables of decoded/thought of activity analysis.

* {{ 686|issue }}
* {{ 232|issue }}

## Sources

* Ainsworth, B. E., Haskell, W. L., Whitt, M. C., Irwin, M. L., Swartz, A. M., Strath, S. J., O'brien, W. L., Bassett, D. R., Jr., Schmitz, K. H., Emplaincourt, P. O., Jacobs, D. R. Jr., & Leon, A. S. (2000, September). Compendium of physical activities: An update of activity codes and MET intensities. Medicine & Science in Sports & Exercise, 32, 498-516. doi:10.1097/00005768-200009001-00009, [can be found here.][source-1]{: target="_blank" }
* Bao L., & Intille S. S. (2004). Activity Recognition from User-Annotated Acceleration Data. In Ferscha, A., & Mattern, F. (Eds.), Pervasive Computing: Second International Conference, PERVASIVE 2004, Vienna Austria, April 21-23, 2004, Proceedings (1-17). Berlin-Heidelberg, Germany: Springer-Verlag. doi: 10.1007/978-3-540-24646-6_1, [can be found here.][source-2]{: target="_blank" }
* [Fitbit Blog][source-3]{: target="_blank"}
* Penzel, T., Kesper, K., Pinnow, I., Becker, H. F., & Vogelmeier, C. (2004, August). Peripheral arterial tonometry, oximetry and actigraphy for ambulatory recording of sleep apnea. Physiological measures, 25(4), 1025-1036. doi:10.1088/0967-3334/25/4/019, [can be found here.][source-4]{: target="_blank" }
* van Hees T. V., & Ekelund, U. (2009, September 1). Novel daily energy expenditure estimation by using objective activity type classification: where do we go from here? Journal of Applied Physiology, 107(3), 639-640. doi:10.1152/japplphysiol.00793.2009, [can be found here.][source-5]{: target="_blank" }
* Yalamanchali, S., Farajian, V., Hamilton, C., Pott, T. R., Samuelson, C. G., & Friedman, M. (2013, December 1). Diagnosis of obstructive sleep apnea by peripheral arterial tonometry. JAMA Otolaryngology - Head & Neck Surgery, 139(12), 1343-1350. doi:10.1001/jamaoto.2013.5338, [can be found here.][source-6]{: target="_blank" }
* Estimation of sleep stages in a healthy adult population from optical plethysmography and accelerometer signals, Z Beattie1, Y Oyang1, A Statan1, A Ghoreyshi1, A Pantelopoulos1, A Russell1 and C Heneghan1, Published 31 October 2017 • © 2017 Institute of Physics and Engineering in Medicine, [can be found here.][source-7]{: target="_blank" }
* Rahimi-Eichi H, Coombs Iii G, Vidal Bustamante CM, Onnela JP, Baker JT, Buckner RL. Open-source Longitudinal Sleep Analysis From Accelerometer Data (DPSleep): Algorithm Development and Validation. JMIR Mhealth Uhealth. 2021 Oct 6;9(10):e29849. doi: 10.2196/29849. PMID: 34612831; PMCID: PMC8529474, [can be found here.][source-8]{: target="_blank" }
* [Another search results.][search-results]{: target="_blank" }

[most-commented-issues]: https://codeberg.org/Freeyourgadget/Gadgetbridge/issues?q=&type=all&sort=mostcomment&state=open&labels=&milestone=0&project=0&assignee=0&poster=0
[source-1]: https://web.archive.org/web/20170215030223/https://www.juststand.org/portals/3/literature/compendium-of-physical-activities.pdf
[source-2]: https://web.archive.org/web/20170809021048/http://robotics.usc.edu/~gaurav/CS546/readings/bao_pc2004.pdf
[source-3]: https://web.archive.org/web/20150910081400/https://blog.fitbit.com/246/
[source-4]: https://www.ncbi.nlm.nih.gov/pubmed/15382839
[source-5]: https://journals.physiology.org/doi/pdf/10.1152/japplphysiol.00793.2009
[source-6]: https://www.ncbi.nlm.nih.gov/pubmed/24158564
[source-7]: https://iopscience.iop.org/article/10.1088/1361-6579/aa9047/meta
[source-8]: https://pubmed.ncbi.nlm.nih.gov/34612831/
[search-results]: https://duckduckgo.com/?q=sleep+detection+accelerometer+open+source+&t=fpas&ia=web
