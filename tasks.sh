#!/usr/bin/env bash
set -euo pipefail

#
#    Copyright (C) 2023 ysfchn
#
#    This file is part of Gadgetbridge.
#
#    Gadgetbridge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Gadgetbridge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

DIST_FOLDER="docs-out"
TOOL_FOLDER="downloads"

# For publishing to:
# https://codeberg.org/Freeyourgadget/gadgetbridge_org
GIT_FOLDER="gadgetbridge_org"
GIT_URL="git@codeberg.org:Freeyourgadget/gadgetbridge_org.git"
GIT_BRANCH="pages"
GIT_MESSAGE="Publish documentation"

# https://stackoverflow.com/a/11114547
SCRIPT_PATH=$(dirname $(realpath -s $0))

check_rye() {
    if [ -x "$(command -v rye)" ]
    then
        echo 0
    else
        echo 1
    fi
}

get_tool_path() {
    TOOL_NAME=$1
    if [ -x "$(command -v $TOOL_NAME)" ]
    then
        echo $TOOL_NAME
    elif [ -f "${TOOL_FOLDER}/${TOOL_NAME}/${TOOL_NAME}" ]
    then
        echo "${TOOL_FOLDER}/${TOOL_NAME}/${TOOL_NAME}"
    else
        echo ""
    fi
}

require_mkdocs() {
    if [ ! -x "$(command -v mkdocs)" ]
    then
        echo
        echo Mkdocs is not properly installed,
        echo run \"./$(basename "$0") deps\" again.
        echo
        exit 1
    fi
}

require_build() {
    if [ ! -d "${DIST_FOLDER}" ]
    then
        echo
        echo Build the documentation first. To do that,
        echo run \"./$(basename "$0") dist\".
        echo
        exit 1
    fi
}

enter_venv() {
    if test -f ./.venv/bin/activate
    then
        . ./.venv/bin/activate
    else
        CHECK=$(check_rye)
        if [ "$CHECK" -eq 0 ]
        then
            deps
            . ./.venv/bin/activate
        else
            echo
            echo No virtualenv does exists and rye is not installed
            echo to create one. Run \"./$(basename "$0") deps\" for more details.
            echo 
            exit 1
        fi
    fi
}

deps() {
    CHECK=$(check_rye)
    if [ "$CHECK" -ne 0 ]
    then
        echo
        echo This command is only available if rye is installed
        echo and added to PATH, see this link to install rye\;
        echo https://rye-up.com/guide/installation
        echo
        echo For other Python version managers, it is possible
        echo to install dependencies with pip as usual, but
        echo the process is not documented here.
        echo
        exit 1
    fi
    rye sync
}

deps_tools() {
    JQ_PATH=$(get_tool_path jq)
    if [ -z "${JQ_PATH}" ]
    then
        echo
        echo This command requires \"jq\" installed to run.
        echo See\; https://jqlang.github.io/jq/
        echo
        exit 1
    fi
    mkdir "${TOOL_FOLDER}" --parents
    "${JQ_PATH}" -Mc '.[] | [.name, .url, .checksum, .command]' optional_tools.json | while IFS= read -r x
    do
        NAME=$(echo $x | jq -r '.[0]')
        URL=$(echo $x | jq -r '.[1]')
        CHECKSUM=$(echo $x | jq -r '.[2]')
        COMMAND=$(echo $x | jq -r '.[3]')
        if [ -z "$(get_tool_path $NAME)" ]
        then
            echo; echo Downloading ${NAME}...; echo
            wget $URL --show-progress -q -O "${TOOL_FOLDER}/${NAME}.out"
            printf '%s %s\n' $CHECKSUM "${TOOL_FOLDER}/${NAME}.out" | sha256sum --check
            mkdir "${TOOL_FOLDER}/${NAME}" --parents
            eval "${COMMAND}"
            chmod +x "${TOOL_FOLDER}/${NAME}/${NAME}"
            rm "${TOOL_FOLDER}/${NAME}.out"
            echo; echo Downloaded \"${NAME}\" to ${TOOL_FOLDER}/${NAME}.
        else
            echo; echo \"${NAME}\" is already exists.
        fi
    done
}

post_dist_optimize() {
    require_build
    MINIFY_BIN=$(get_tool_path minify)
    JPEGOPTIM_BIN=$(get_tool_path jpegoptim)
    PNGQUANT_BIN=$(get_tool_path pngquant)
    if [ -n "${MINIFY_BIN}" ]; then
        echo; echo Minifying the generated pages...; echo
        "${MINIFY_BIN}" --all --html-keep-whitespace -r "${DIST_FOLDER}/" --output "${DIST_FOLDER}"
    else
        echo; echo \"minify\" is not available, skipping to run it...
    fi
    if [ -n "${JPEGOPTIM_BIN}" ]; then
        echo; echo Optimizing JPEGs...; echo
        "${JPEGOPTIM_BIN}" -f --totals --strip-all $(find "${DIST_FOLDER}" -name "*.jpg" -o -name "*.jpeg")
    else
        echo; echo \"jpegoptim\" is not available, skipping to run it...
    fi
    if [ -n "${PNGQUANT_BIN}" ]; then
        echo; echo Optimizing PNGs...;
        "${PNGQUANT_BIN}" --strip --force --ext .png $(find "${DIST_FOLDER}" -name "*.png")
        echo Optimized $(find "${DIST_FOLDER}" -name "*.png" | wc -l) PNG images.
    else
        echo; echo \"pngquant\" is not available, skipping to run it...
    fi
    echo; echo Optimize finished.
}

get_build_info() {
    # Create a JSON file to track when the documentation was built,
    # so this information can be pulled from the documentation itself,
    # and it might be useful to learn if documentation is outdated.
    OUTPUT="
    {
        \"commit_hash\": \"$(git log -1 --format="%H")\",
        \"commit_unix\": \"$(git log -1 --format="%at")\",
        \"build_unix\": \"$(date +%s)\"
    }
    "
    echo "${OUTPUT}" | sed 's/^[[:space:]]*//'
}

post_dist_extra() {
    require_build
    # Delete _misc from dist folder because it just contains source code
    # used for documentation, so we won't need that in generated website.
    rm -rf "${DIST_FOLDER}/_misc"

    # Copy .domains file to output folder.
    cp ".domains" "${DIST_FOLDER}/.domains"

    # Workaround for https://github.com/mkdocs/mkdocs/issues/3551
    sed -i 's/Index - Gadgetbridge/Gadgetbridge/g' "${DIST_FOLDER}/index.html"
    cp "${DIST_FOLDER}/assets/static/og_image_index.png" "${DIST_FOLDER}/assets/images/social/index.png"

    BUILD_INFO=$(get_build_info)
    rm -rf "${DIST_FOLDER}/docs_info.json"
    echo "${BUILD_INFO}" >> "${DIST_FOLDER}/docs_info.json"
}

serve() {
    enter_venv
    require_mkdocs
    mkdocs serve
}

dist() {
    enter_venv
    require_mkdocs
    rm -rf "${DIST_FOLDER}/"
    BUILD_SOCIAL_CARDS=true mkdocs build -d "${DIST_FOLDER}"
    post_dist_extra
    post_dist_optimize
}

clean() {
    rm -rf "${DIST_FOLDER}/"
}

publish() {
    enter_venv
    require_build
    if [ -d "${GIT_FOLDER}" ]
    then 
        cd "${GIT_FOLDER}"
        git pull origin "${GIT_BRANCH}"
    else
        git clone --branch "${GIT_BRANCH}" "${GIT_URL}" "${GIT_FOLDER}"
        cd "${GIT_FOLDER}"
    fi
    # Don't use "mkdocs gh-deploy" command directly because apperantly it builds
    # the documentation again, which is not the case that we want due to
    # all post-processing after builds are lost, such as optimizing pages/images,
    # so we use "ghp_import" which is what Mkdocs uses underneath.
    #
    # https://github.com/mkdocs/mkdocs/blob/646987da4502becf346bfaabb6ba40934307399b/mkdocs/commands/gh_deploy.py#L100
    # https://github.com/c-w/ghp-import/blob/5219f00fc83606ff426b978a9920ea746923dcb7/ghp_import.py#L199
    python -m ghp_import -n -b "${GIT_BRANCH}" -o -r origin -s -m "${GIT_MESSAGE}" "${SCRIPT_PATH}/${DIST_FOLDER}"
    git push -u origin "${GIT_BRANCH}" --force
}

# Publishing to Netlify is provided as an alternative to Codeberg Pages, 
# because unfortunately Codeberg Pages is less reliable than common CDNs, 
# so it may get difficult to preview the documentation right away.
publish_netlify() {
    enter_venv
    require_build
    netlify deploy --dir "${DIST_FOLDER}" --alias "git-$(git log -1 --format="%h")"
}

if [ "$#" -eq 0 ]
then
    echo
    echo "usage: ./$(basename "$0") TASK"
    echo
    echo "common tasks:"
    echo "    deps                Installs required Python dependencies & creates venv"
    echo "    deps_tools          Downloads optional command line tools to minify generated files"
    echo "    dist                Build documentation"
    echo "    publish             Publish the already built documentation to Gadgetbridge git repo"
    echo "    publish_netlify     Publish the already built documentation to Netlify"
    echo "    serve               Live-preview documentation"
    echo
    exit 1
else
    for var in "$@"
    do
        $var
    done
fi
